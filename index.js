var app = {
    history: [] ,
    storage: window.localStorage,

    initialize: function() {
      document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady: function() {
      this.showPage('home');
      document.addEventListener('backbutton', this.onBackButton, false);
      // $("button#btnHomeToModePage").click(function() {      app.changePage($(this), 'selectGame')  });
      // $("button#btn5x5GamePage").click(function() {         app.changePage($(this), 'game', 5)  });
      // $("button#btn6x6GamePage").click(function() {         app.changePage($(this), 'game', 6)  });
      // $("button#btn7x7GamePage").click(function() {         app.changePage($(this), 'game', 7)  });
      // $("button#btn8x8GamePage").click(function() {         app.changePage($(this), 'game', 8)  });
      // $("button#btn9x9GamePage").click(function() {         app.changePage($(this), 'game', 9)  });
      // $("button#btn10x10GamePage").click(function() {       app.changePage($(this), 'game', 10)  });
      $("button#btnBack").click(function() {                app.changePage($(this), app.history.pop())  });
      $("button#btnSettings").click(function() {            app.changePage($(this), 'settings')  });
      $('#carouselExampleInterval').on('slide.bs.carousel', function (el) {   game.setDim(el.to+5)   });
      $("button#btnGamePage").click(function() {         app.changePage($(this), 'game')  });

    },

    onBackButton: function() {
      if(app.history.length > 0){
        app.changePage($("button#btnBack"), app.history.pop());
      }else{
        navigator.app.exitApp();
      }
    },

    showPage: function(pageName) {
       var page = $(".page#" + pageName + "Page");
       page.show();
    },

    changePage: function(el, pageName, dim) {
      var page = $(".page:visible");
      var pageId = page.attr('id');
      if(el.attr('id') != "btnBack"){
        this.history.push(pageId.replace('Page', ''));
      }
      page.hide();
      if(pageName == "game"){
        game.resetField();
        if(typeof dim === "undefined"){
          dim=game.dim;
        }
        game.createField(dim);
      }
      if(pageName == "home"){
        $("#btnBack").hide();
      }else{
        $("#btnBack").show();
      }
      
      if(pageName == "settings"){
          $("#btnSettings").hide();
        }else{
          $("#btnSettings").show();
        }

      this.showPage(pageName);
    }
};



var game = {
    field: [],
    dim: 5,
    count: 1,
    posX: 0,
    posY: 0,
    //Si può utilizzare invece di una matrice, un cont di elementi "colorati"

    createField: function(dim) {
      this.dim = dim;
      var width = 100/this.dim;
      var tableField = $("#field");
      for(var i=0; i<this.dim; i++) {
        field[i] = [];
        var tr=$('<tr></tr>');
        for(var j=0; j<this.dim; j++) {
          field[i][j] = 0;
          tr.append('<td class="cell border" style="width: '+width+'%"><div class="content" id="celX'+i+'Y'+j+'"></div></td>');
        }
        tableField.append(tr);
      }
      field[0][0] = 1;
      $("#celX0Y0").css({"background":"#EFDD62"});
      $("dd#move").text(this.count);
      $("dd#record").text(record.getData(this.dim));
      for(var i=0; i<this.dim; i++) {
        for(var j=0; j<this.dim; j++) {
          $("div#celX"+i+"Y"+j).click(function() { game.getPosition($(this)) });
        }
      }
    },

    setDim: function(dim) {
      this.dim=dim;
    },

    getPosition: function(el) {
      var str = el.attr('id');
      //ATTENZIONE! Sopra la 10x10 x ed y prendono solamente un char
      str = str.slice(4);
      var x=parseInt(str[0]);
      var y=parseInt(str[2]);
      if((this.posX+2==x || this.posX-2==x)&&(this.posY+1==y || this.posY-1==y)){
        if(field[x][y]==0){
          this.execMove(el,x,y);
        }
      }else if ((this.posX+1==x || this.posX-1==x)&&(this.posY+2==y || this.posY-2==y)) {
        if(field[x][y]==0){
          this.execMove(el,x,y);
        }
      }
      if(this.findNextMoves()!=true){
        app.changePage(el ,"finish");
        if(this.count<this.dim*this.dim){
          $("#textResult").text("Game over!");
        }else{
          $("#textResult").text("Win!");
        }
        record.save(this.dim,this.count);
      }
    },

    execMove: function(el,x,y) {
      $("div#celX"+this.posX+"Y"+this.posY).css({"background":"#F2F2F2"});
      el.css({"background":"#EFDD62"});
      this.posX=x;
      this.posY=y;
      field[x][y]=1;
      this.count+=1;
      $("dd#move").text(this.count);
    },

    checkNextMove: function(x,y) {
      if(x>=0 && x<this.dim && y>=0 && y<this.dim){
        if((this.posX+2==x || this.posX-2==x)&&(this.posY+1==y || this.posY-1==y)){
          if(field[x][y]==0){
            return true;
          }
        }else if ((this.posX+1==x || this.posX-1==x)&&(this.posY+2==y || this.posY-2==y)) {
          if(field[x][y]==0){
            return true;
          }
        }
      }
      return false;
    },

    findNextMoves: function() {
      if(this.checkNextMove(this.posX+2, this.posY+1)==true){
        return true;
      }else if (this.checkNextMove(this.posX+2, this.posY-1)==true) {
        return true;
      }else if (this.checkNextMove(this.posX-2, this.posY+1)==true) {
        return true;
      }else if (this.checkNextMove(this.posX-2, this.posY-1)==true) {
        return true;
      }else if (this.checkNextMove(this.posX+1, this.posY+2)==true) {
        return true;
      }else if (this.checkNextMove(this.posX-1, this.posY+2)==true) {
        return true;
      }else if (this.checkNextMove(this.posX+1, this.posY-2)==true) {
        return true;
      }else if (this.checkNextMove(this.posX-1, this.posY-2)==true) {
        return true;
      }else{
        return false;
      }
    },

    resetField: function() {
      $("#field").empty();
      field = [];
      this.posX=0;
      this.posY=0;
      this.count=1;
    }
};



var record = {
    data: {field: "",  record: "0"},

    save: function(field, record) {
      if(this.getData(field) < record){
        this.data.field = field;
        this.data.record = record;
        app.storage.setItem(this.data.field, this.data.record);
      }
    },

    getData: function(field) {
      var record = app.storage.getItem(field);
      return record;
    },

    delete: function(field){
      app.storage.removeItem(field);
    }
};



$(document).ready(function() {
	app.initialize();
});
